import os
import glob
import in_place

for filename in glob.iglob('../ext/m.livejournal.com/**/*', recursive=True):
    if os.path.isfile(filename):
        with in_place.InPlace(filename) as file:
            data = file.read()
            data = data.replace('<?xml version="1.0" encoding="utf-8"?>', '')
            file.write(data)
